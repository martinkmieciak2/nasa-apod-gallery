import { all, put, takeLatest } from 'redux-saga/effects';

import { apodActions } from '../apod';
import { startupActions } from '.';

// eslint-disable-next-line @typescript-eslint/no-empty-function
export function* handleStartup() {
  yield put(apodActions.readFavouriteImages());
}

export function* watchStartup() {
  yield all([takeLatest(startupActions.startup, handleStartup)]);
}
