import { actionCreator } from '../helpers';
import { ApodImagesMap, NormalizedApodImage } from './apod.types';

const { createAction } = actionCreator('APOD');

export const fetch = createAction<undefined>('FETCH');
export const fetchSuccess = createAction<NormalizedApodImage>('FETCH_SUCCESS');
export const fetchFailure = createAction<undefined>('FETCH_FAILURE');

export const save = createAction<undefined>('SAVE');

export const readFavouriteImages = createAction<undefined>('READ_FAVOURITE_IMAGES');
export const setFavouriteImages = createAction<ApodImagesMap>('SET_FAVOURITE_IMAGES');

export const clearCurrentImage = createAction<undefined>('CLEAR_CURRENT_IMAGE');
