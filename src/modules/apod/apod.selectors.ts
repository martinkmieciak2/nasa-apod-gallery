import { createSelector } from '@reduxjs/toolkit';
import { memoize } from 'lodash';
import { values } from 'ramda';

import { GlobalState } from '../../config/reducers';

export const selectApodDomain = (state: GlobalState) => state.apod;

export const selectApodCurrentImage = createSelector(selectApodDomain, (apod) => apod.currentImage);

export const selectApodImages = createSelector(selectApodDomain, (apod) => apod.images);

export const selectApodImageDataById = memoize((imageId: string) =>
  createSelector(selectApodImages, (images) => images[imageId])
);

export const selectApodCurrentImageData = createSelector(
  selectApodCurrentImage,
  selectApodImages,
  (currentImage, images) => (currentImage ? images[currentImage] : null)
);

export const selectApodIsLoading = createSelector(selectApodDomain, (apod) => apod.isLoading);

export const selectApodFavouriteImages = createSelector(selectApodDomain, (apod) => apod.favouriteImages);

export const selectApodFavouriteImagesList = createSelector(selectApodFavouriteImages, values);
