import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { produce } from 'immer';
import { mergeDeepRight } from 'ramda';

import * as apodActions from './apod.actions';
import { ApodImagesMap, ApodState, NormalizedApodImage } from './apod.types';

export const INITIAL_STATE: ApodState = {
  currentImage: null,
  images: {},
  favouriteImages: {},
  isLoading: false,
};

const handleFetch = (state: ApodState) =>
  produce(state, (draftState) => {
    draftState.isLoading = true;
  });

const handleFetchSuccess = (state: ApodState, { payload }: PayloadAction<NormalizedApodImage>) =>
  produce(state, (draftState) => {
    const {
      entities: { images },
      result,
    } = payload;

    draftState.images = mergeDeepRight(draftState.images, images);
    draftState.currentImage = result;
    draftState.isLoading = false;
  });

const handleFetchFailure = (state: ApodState) =>
  produce(state, (draftState) => {
    draftState.isLoading = false;
  });

const handleSetFavouriteImages = (state: ApodState, { payload }: PayloadAction<ApodImagesMap>) =>
  produce(state, (draftState) => {
    draftState.favouriteImages = payload;
  });

const handleClearCurrentImage = (state: ApodState) =>
  produce(state, (draftState) => {
    draftState.currentImage = null;
  });

export const reducer = createReducer(INITIAL_STATE, (builder) => {
  builder.addCase(apodActions.fetch, handleFetch);
  builder.addCase(apodActions.fetchSuccess, handleFetchSuccess);
  builder.addCase(apodActions.fetchFailure, handleFetchFailure);
  builder.addCase(apodActions.setFavouriteImages, handleSetFavouriteImages);
  builder.addCase(apodActions.clearCurrentImage, handleClearCurrentImage);
});
