export enum MediaType {
  IMAGE = 'image',
}

export interface ApodImage {
  title: string;
  url: string;
  hdurl: string;
  explanation: string;
  date: string;
  copyright: string;
  media_type: MediaType;
}

export type ApodImagesMap = Record<string, ApodImage>;

export interface ApodState {
  currentImage: string | null;
  isLoading: boolean;
  favouriteImages: Record<string, ApodImage>;
  images: ApodImagesMap;
}

export interface NormalizedApodImage {
  result: string;
  entities: {
    images: Record<string, ApodImage>;
  };
}
