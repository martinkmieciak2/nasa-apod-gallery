import { schema } from 'normalizr';

export const imageSchema = new schema.Entity('images', {}, { idAttribute: 'url' });
