import { MediaType } from './apod.types';

export const SUPPORTED_MEDIA_TYPES = [MediaType.IMAGE];
