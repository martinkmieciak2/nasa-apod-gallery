import { all, put, select, takeEvery, takeLatest } from 'redux-saga/effects';
import { normalize } from 'normalizr';
import { mergeDeepRight } from 'ramda';
import { push } from 'react-router-redux';
import { PayloadAction } from '@reduxjs/toolkit';

import { ROUTES } from '../../routes/app.constants';
import { selectLocalesLanguage } from '../locales/locales.selectors';
import { reportError } from '../../shared/utils/reportError';
import { api } from '../../shared/services/api';
import { seenImagesCollection } from '../../shared/services/firebase';
import { LocalStorageKey, readLocalStorageObject, saveLocalStorageObject } from '../../shared/services/storage';
import * as apodActions from './apod.actions';
import { SUPPORTED_MEDIA_TYPES } from './apod.constants';
import { imageSchema } from './apod.schema';
import { ApodImage, NormalizedApodImage } from './apod.types';
import {
  selectApodCurrentImage,
  selectApodCurrentImageData,
  selectApodFavouriteImages,
  selectApodImages,
} from './apod.selectors';

function* getWasImageShownBefore(url: string) {
  try {
    const ref = seenImagesCollection.where('url', '==', url);
    const data = yield ref.get();
    return !data.empty;
  } catch (error) {
    reportError(error);
    return false;
  }
}

function* getShouldRefetchImage({ url, media_type }: ApodImage) {
  try {
    const images = yield select(selectApodImages);

    return !SUPPORTED_MEDIA_TYPES.includes(media_type) || images[url] || (yield getWasImageShownBefore(url));
  } catch (error) {
    reportError(error);
    return false;
  }
}

function* fetch() {
  try {
    const {
      data: [item],
    } = yield api.get('/', { params: { count: 1 } });

    const shouldRefetchImage = yield getShouldRefetchImage(item);

    if (shouldRefetchImage) {
      yield put(apodActions.fetch());
      return;
    }

    const normalized = normalize(item, imageSchema) as NormalizedApodImage;

    yield put(apodActions.fetchSuccess(normalized));
  } catch (error) {
    reportError(error);
    yield put(apodActions.fetchFailure());
  }
}

function* fetchSuccess({ payload }: PayloadAction<NormalizedApodImage>) {
  try {
    const { result: url } = payload;
    yield seenImagesCollection.add({ url });
  } catch (error) {
    reportError(error);
  }
}

function* save() {
  try {
    const currentImage = yield select(selectApodCurrentImage);
    const currentImageData = yield select(selectApodCurrentImageData);
    const favouriteImages = yield select(selectApodFavouriteImages);

    if (!currentImage) {
      return;
    }

    const newFavouriteImages = mergeDeepRight(favouriteImages, { [currentImage]: currentImageData });

    saveLocalStorageObject<ApodImage>(LocalStorageKey.APOD_FAVOURITE_IMAGE, newFavouriteImages);

    yield put(apodActions.setFavouriteImages(newFavouriteImages));
    const language = yield select(selectLocalesLanguage);
    yield put(push(`${language}${ROUTES.savedImages}`));
  } catch (error) {
    reportError(error);
  }
}

function* readFavouriteImages() {
  try {
    const favouriteImages = readLocalStorageObject<ApodImage>(LocalStorageKey.APOD_FAVOURITE_IMAGE);

    yield put(apodActions.setFavouriteImages(favouriteImages));
  } catch (error) {
    reportError(error);
  }
}

export function* watchApod() {
  yield all([
    takeLatest(apodActions.fetch, fetch),
    takeEvery(apodActions.fetchSuccess, fetchSuccess),
    takeLatest(apodActions.readFavouriteImages, readFavouriteImages),
    takeEvery(apodActions.save, save),
  ]);
}
