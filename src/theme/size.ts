import { Breakpoint, responsiveValue } from './media';

export const header = 80;

export const contentHorizontalPadding = responsiveValue(36, {
  [Breakpoint.TABLET]: 48,
});

export const contentVerticalPadding = responsiveValue(15, {
  [Breakpoint.TABLET]: 24,
});
