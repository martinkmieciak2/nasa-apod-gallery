export const white = '#ffffff';
export const black = '#000000';
export const blackAlpha30 = 'rgba(0, 0, 0, 0.3)';

export const border = '#80809B';
export const disabled = '#DDDDE0';
export const primary = '#42f272';
export const secondary = '#2249f9';
export const error = '#fa7265';
