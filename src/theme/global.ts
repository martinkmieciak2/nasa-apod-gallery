import { createGlobalStyle } from 'styled-components';

import { fontFamily } from './font';
import { color } from './index';

export const GlobalStyle = createGlobalStyle`
 @keyframes animatedBackground {
    from {
      background-position: 0 0;
    }
    to {
      background-position: 100vw 0;
    }
  }

  html {
    box-sizing: border-box;
    font-family: ${fontFamily.primary};
  }

  body {
    background-color: ${color.black};
  }

  *, *:before, *:after {
    box-sizing: inherit;
  }
`;
