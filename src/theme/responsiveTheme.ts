import { map } from 'ramda';
import { DefaultTheme } from 'styled-components';

// eslint-disable-next-line @typescript-eslint/ban-types
const deepMap = (obj: object, fn: Function): object =>
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  map((val) => (typeof val === 'object' ? deepMap(val, fn) : fn(val)), obj);
// eslint-disable-next-line @typescript-eslint/ban-types
const parseThemeValue = (value: string | Function): object => (typeof value !== 'function' ? value : value());

// eslint-disable-next-line @typescript-eslint/ban-types
export default (theme: object): DefaultTheme => deepMap(theme, parseThemeValue) as DefaultTheme;
