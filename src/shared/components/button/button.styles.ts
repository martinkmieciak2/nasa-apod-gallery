import styled from 'styled-components';

import { border, color } from '../../../theme';

export const Container = styled.button`
  padding: 0 20px;
  border: ${border.regular};
  border-radius: 25px;
  height: 30px;
  color: ${color.white};
  text-transform: uppercase;
  font-size: 10px;
  cursor: pointer;
  background-color: ${color.blackAlpha30};
  transition: background-color 0.3s ease-in-out;

  &:focus {
    outline: 0;
  }

  &:hover {
    background-color: ${color.black};
  }
`;
