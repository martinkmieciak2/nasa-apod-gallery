export function saveLocalStorageObject<T>(key: string, data: Record<string, T>): void {
  localStorage.setItem(key, JSON.stringify(data));
}

export function readLocalStorageObject<T>(key: string): Record<string, T> {
  const data = localStorage.getItem(key);
  return !data ? {} : JSON.parse(data);
}

export enum LocalStorageKey {
  APOD_FAVOURITE_IMAGE = 'apod-favourite-image',
}
