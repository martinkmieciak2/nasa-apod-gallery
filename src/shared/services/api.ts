import axios from 'axios';

const baseUrl = process.env.REACT_APP_NASA_APOD_API_URL;

if (!baseUrl) {
  throw new Error('NASA_APOD_API_URL env is missing');
}

export const api = axios.create({
  baseURL: baseUrl,
  params: {
    api_key: process.env.REACT_APP_NASA_APOD_API_KEY,
  },
});
