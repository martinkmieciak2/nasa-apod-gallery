import React, { useCallback, useEffect, useRef } from 'react';
import { Helmet } from 'react-helmet-async';
import { useIntl } from 'react-intl';
import Loader from 'react-loader-spinner';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';

import { apodActions } from '../../modules/apod';
import { selectApodCurrentImageData, selectApodIsLoading } from '../../modules/apod/apod.selectors';
import { renderWhenTrueOtherwise } from '../../shared/utils/rendering';
import { color } from '../../theme';
import { ROUTES } from '../app.constants';
import { useLanguageRouter } from '../../shared/components/languageSwitcher/useLanguageRouter.hook';
import {
  Container,
  Image,
  Wrapper,
  Content,
  Title,
  Description,
  Button,
  ButtonsWrapper,
  ImagePlaceholder,
  LoaderWrapper,
} from './home.styles';

export const Home = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [{ language }] = useLanguageRouter();
  const intl = useIntl();
  const imgRef = useRef<HTMLImageElement>(null);
  const currentImage = useSelector(selectApodCurrentImageData);
  const isLoading = useSelector(selectApodIsLoading);

  const fetch = useCallback(() => {
    dispatch(apodActions.fetch());
  }, [dispatch]);

  const save = useCallback(() => {
    dispatch(apodActions.save());
  }, [dispatch]);

  useEffect(() => {
    fetch();

    return () => {
      dispatch(apodActions.clearCurrentImage());
    };
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const goToSavedImages = useCallback(() => {
    history.push(`${language}${ROUTES.savedImages}`);
  }, [history, language]);

  const renderContent = renderWhenTrueOtherwise(
    () => {
      const { url, date, title, copyright } = currentImage ?? {};

      return (
        <>
          <Image ref={imgRef} src={url} />
          <Content>
            <Title>{title}</Title>
            <Description>
              {copyright ? `${copyright} ©` : ''}&nbsp;{date}
            </Description>
          </Content>
        </>
      );
    },
    () => (
      <LoaderWrapper>
        <Loader type="Rings" color={color.white} height={80} width={80} timeout={1000} />
      </LoaderWrapper>
    )
  );

  return (
    <Container>
      <Helmet
        title={intl.formatMessage({
          defaultMessage: 'Homepage',
          description: 'Home / page title',
        })}
      />
      <Wrapper>
        <ImagePlaceholder />
        {renderContent(!isLoading)}
        <ButtonsWrapper>
          <Button onClick={fetch}>
            {intl.formatMessage({
              defaultMessage: 'Next',
              description: 'Home / next button',
            })}
          </Button>
          <Button onClick={save}>
            {intl.formatMessage({
              defaultMessage: 'Save',
              description: 'Home / save button',
            })}
          </Button>
          <Button onClick={goToSavedImages}>
            {intl.formatMessage({
              defaultMessage: 'Saved',
              description: 'Home / saved button',
            })}
          </Button>
        </ButtonsWrapper>
      </Wrapper>
    </Container>
  );
};
