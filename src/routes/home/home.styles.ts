import styled from 'styled-components';
import { H1, Paragraph } from '../../theme/typography';
import { color, size } from '../../theme';
import { Button as ButtonBase } from '../../shared/components/button';

export const Container = styled.div`
  position: relative;
  overflow: hidden;
  width: 100%;
  height: 100vh;
  border-radius: 10px;
`;

export const Wrapper = styled.div`
  padding: 10px;
  width: 100%;
  height: 100%;
  position: relative;
`;

export const Image = styled.img`
  position: relative;
  z-index: 1;
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 15px;
`;

export const Content = styled.div`
  position: absolute;
  z-index: 1;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: ${size.contentVerticalPadding}px ${size.contentHorizontalPadding}px;
`;

export const Title = styled(H1)`
  text-shadow: 0 1px 0 #ccc, 0 2px 0 #c9c9c9, 0 3px 0 #bbb, 0 4px 0 #b9b9b9, 0 5px 0 #aaa, 0 6px 1px rgba(0, 0, 0, 0.1),
    0 0 5px rgba(0, 0, 0, 0.1), 0 1px 3px rgba(0, 0, 0, 0.3), 0 3px 5px rgba(0, 0, 0, 0.2),
    0 5px 10px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.2), 0 20px 20px rgba(0, 0, 0, 0.15);

  text-transform: uppercase;
  letter-spacing: 0.05em;
`;

export const Description = styled(Paragraph)`
  max-width: 300px;
  text-align: center;
`;

export const ButtonsWrapper = styled.div`
  position: absolute;
  bottom: 22px;
  width: 100%;
  display: flex;
  justify-content: center;
  z-index: 2;
`;

export const Button = styled(ButtonBase)`
  margin: 0 5px;
`;

export const ImagePlaceholder = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  z-index: 0;
  width: 100%;
  height: 100%;
  background-image: linear-gradient(90deg, #e4e4e4 0%, #f1f1f1 40%, #ededed 60%, #e4e4e4 100%);
  background-position: 0px 0px;
  background-repeat: repeat;
  animation: animatedBackground 5s linear infinite;
  border-radius: 20px;
`;

export const LoaderWrapper = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 2;
  background-color: ${color.black};
`;
