export const ROUTES = {
  home: '/',
  savedImages: '/saved-images',
  notFound: '/404',
};
