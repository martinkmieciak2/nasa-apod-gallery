import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router';
import { isEmpty } from 'ramda';

import { selectApodFavouriteImagesList } from '../../modules/apod/apod.selectors';
import { Button } from '../../shared/components/button';
import { ROUTES } from '../app.constants';
import { renderWhenTrue } from '../../shared/utils/rendering';
import {
  Container,
  List,
  ImageContainer,
  ImageWrapper,
  Image,
  TextLine,
  ButtonsWrapper,
  EmptyStateWrapper,
  EmptyStateText,
} from './savedImages.styles';

export const SavedImages = () => {
  const intl = useIntl();
  const history = useHistory();
  const images = useSelector(selectApodFavouriteImagesList);

  const goToHome = useCallback(() => {
    history.push(ROUTES.home);
  }, [history]);

  const renderImage = useCallback(
    ({ url, date, copyright }) => (
      <ImageContainer key={url}>
        <ImageWrapper>
          <Image src={url} />
        </ImageWrapper>
        <TextLine>{copyright ? `${copyright} ©` : ''}</TextLine>
        <TextLine>{date}</TextLine>
      </ImageContainer>
    ),
    []
  );

  const renderPlaceholder = renderWhenTrue(() => (
    <EmptyStateWrapper>
      <EmptyStateText>
        {intl.formatMessage({
          defaultMessage: 'No saved images',
          description: 'SavedImages / empty placeholder',
        })}
      </EmptyStateText>
    </EmptyStateWrapper>
  ));

  return (
    <Container>
      {renderPlaceholder(isEmpty(images))}
      <List>{images.map(renderImage)}</List>
      <ButtonsWrapper>
        <Button onClick={goToHome}>
          {intl.formatMessage({
            defaultMessage: 'Home',
            description: 'SavedImages / home button',
          })}
        </Button>
      </ButtonsWrapper>
    </Container>
  );
};
