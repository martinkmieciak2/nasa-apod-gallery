import styled from 'styled-components';
import * as size from '../../theme/size';
import { color, fontFamily } from '../../theme';

export const Container = styled.div`
  padding-bottom: 100px;
`;

export const List = styled.div`
  max-width: 1000px;
  margin: 0 auto;
  padding: ${size.contentVerticalPadding}px ${size.contentHorizontalPadding}px;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const ImageContainer = styled.div`
  width: calc(100% / 3);
  padding: 10px;
`;

export const ImageWrapper = styled.div`
  position: relative;
  width: 100%;
  padding-bottom: 100%;
  background-image: linear-gradient(90deg, #e4e4e4 0%, #f1f1f1 40%, #ededed 60%, #e4e4e4 100%);
  background-position: 0px 0px;
  background-repeat: repeat;
  animation: animatedBackground 5s linear infinite;
  border-radius: 15px;
`;

export const Image = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 15px;
`;

export const TextLine = styled.span`
  font-family: ${fontFamily.primary};
  color: ${color.white};
  display: block;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  margin-top: 7px;
  font-size: 12px;
`;

export const EmptyStateWrapper = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: ${size.contentVerticalPadding}px ${size.contentHorizontalPadding}px;
`;

export const EmptyStateText = styled.span`
  font-family: ${fontFamily.primary};
  color: ${color.white};
  display: block;
  font-size: 16px;
`;

export const ButtonsWrapper = styled.div`
  position: fixed;
  bottom: 22px;
  width: 100%;
  display: flex;
  justify-content: center;
  z-index: 2;
`;
