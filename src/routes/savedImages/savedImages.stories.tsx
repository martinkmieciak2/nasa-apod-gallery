import React from 'react';
import { Story } from '@storybook/react';

import { SavedImages } from './savedImages.component';

const Template: Story = (args) => {
  return <SavedImages {...args} />;
};

export default {
  title: 'SavedImages',
  component: SavedImages,
};

export const Default = Template.bind({});
Default.args = { children: 'text' };
